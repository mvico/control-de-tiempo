# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Configuracion.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Configuracion(object):
    def setupUi(self, Configuracion):
        Configuracion.setObjectName(_fromUtf8("Configuracion"))
        Configuracion.resize(400, 91)
        self.verticalLayout = QtGui.QVBoxLayout(Configuracion)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.groupBox = QtGui.QGroupBox(Configuracion)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.radioPrincipal = QtGui.QRadioButton(self.groupBox)
        self.radioPrincipal.setChecked(True)
        self.radioPrincipal.setObjectName(_fromUtf8("radioPrincipal"))
        self.verticalLayout_2.addWidget(self.radioPrincipal)
        self.radioSecundario = QtGui.QRadioButton(self.groupBox)
        self.radioSecundario.setChecked(False)
        self.radioSecundario.setObjectName(_fromUtf8("radioSecundario"))
        self.verticalLayout_2.addWidget(self.radioSecundario)
        self.verticalLayout.addWidget(self.groupBox)

        self.retranslateUi(Configuracion)
        QtCore.QMetaObject.connectSlotsByName(Configuracion)

    def retranslateUi(self, Configuracion):
        Configuracion.setWindowTitle(_translate("Configuracion", "Configuración", None))
        self.groupBox.setTitle(_translate("Configuracion", "Configuración de conexión", None))
        self.radioPrincipal.setText(_translate("Configuracion", "Servidor principal", None))
        self.radioSecundario.setText(_translate("Configuracion", "Servidor secundario", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Configuracion = QtGui.QWidget()
    ui = Ui_Configuracion()
    ui.setupUi(Configuracion)
    Configuracion.show()
    sys.exit(app.exec_())

