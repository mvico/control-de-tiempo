# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Principal.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(937, 698)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("icono.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setEnabled(True)
        self.tabWidget.setMinimumSize(QtCore.QSize(667, 397))
        self.tabWidget.setTabShape(QtGui.QTabWidget.Rounded)
        self.tabWidget.setElideMode(QtCore.Qt.ElideNone)
        self.tabWidget.setDocumentMode(False)
        self.tabWidget.setTabsClosable(False)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tabRecorrido = QtGui.QWidget()
        self.tabRecorrido.setObjectName(_fromUtf8("tabRecorrido"))
        self.gridLayout_7 = QtGui.QGridLayout(self.tabRecorrido)
        self.gridLayout_7.setObjectName(_fromUtf8("gridLayout_7"))
        self.groupBox_5 = QtGui.QGroupBox(self.tabRecorrido)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_5.sizePolicy().hasHeightForWidth())
        self.groupBox_5.setSizePolicy(sizePolicy)
        self.groupBox_5.setObjectName(_fromUtf8("groupBox_5"))
        self.gridLayout_5 = QtGui.QGridLayout(self.groupBox_5)
        self.gridLayout_5.setObjectName(_fromUtf8("gridLayout_5"))
        self.label_3 = QtGui.QLabel(self.groupBox_5)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout_5.addWidget(self.label_3, 0, 1, 1, 1)
        self.spinDuracionTotalRecorrido = QtGui.QSpinBox(self.groupBox_5)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinDuracionTotalRecorrido.sizePolicy().hasHeightForWidth())
        self.spinDuracionTotalRecorrido.setSizePolicy(sizePolicy)
        self.spinDuracionTotalRecorrido.setButtonSymbols(QtGui.QAbstractSpinBox.PlusMinus)
        self.spinDuracionTotalRecorrido.setObjectName(_fromUtf8("spinDuracionTotalRecorrido"))
        self.gridLayout_5.addWidget(self.spinDuracionTotalRecorrido, 0, 0, 1, 1)
        self.gridLayout_7.addWidget(self.groupBox_5, 1, 0, 1, 1)
        self.groupBox_3 = QtGui.QGroupBox(self.tabRecorrido)
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.gridLayout = QtGui.QGridLayout(self.groupBox_3)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(self.groupBox_3)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 1, 1, 1)
        self.spinTiempoAntesRecorrido = QtGui.QSpinBox(self.groupBox_3)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinTiempoAntesRecorrido.sizePolicy().hasHeightForWidth())
        self.spinTiempoAntesRecorrido.setSizePolicy(sizePolicy)
        self.spinTiempoAntesRecorrido.setButtonSymbols(QtGui.QAbstractSpinBox.PlusMinus)
        self.spinTiempoAntesRecorrido.setObjectName(_fromUtf8("spinTiempoAntesRecorrido"))
        self.gridLayout.addWidget(self.spinTiempoAntesRecorrido, 0, 0, 1, 1)
        self.spinTiempoDespuesRecorrido = QtGui.QSpinBox(self.groupBox_3)
        self.spinTiempoDespuesRecorrido.setButtonSymbols(QtGui.QAbstractSpinBox.PlusMinus)
        self.spinTiempoDespuesRecorrido.setObjectName(_fromUtf8("spinTiempoDespuesRecorrido"))
        self.gridLayout.addWidget(self.spinTiempoDespuesRecorrido, 1, 0, 1, 1)
        self.label_2 = QtGui.QLabel(self.groupBox_3)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 1, 1, 1, 1)
        self.gridLayout_7.addWidget(self.groupBox_3, 2, 0, 1, 1)
        self.groupBox = QtGui.QGroupBox(self.tabRecorrido)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.formLayout = QtGui.QFormLayout(self.groupBox)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_5 = QtGui.QLabel(self.groupBox)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_5)
        self.timeInicioRecorrido = QtGui.QTimeEdit(self.groupBox)
        self.timeInicioRecorrido.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.timeInicioRecorrido.setCorrectionMode(QtGui.QAbstractSpinBox.CorrectToPreviousValue)
        self.timeInicioRecorrido.setObjectName(_fromUtf8("timeInicioRecorrido"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.timeInicioRecorrido)
        self.label_6 = QtGui.QLabel(self.groupBox)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_6)
        self.timeFinalRecorrido = QtGui.QTimeEdit(self.groupBox)
        self.timeFinalRecorrido.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.timeFinalRecorrido.setCorrectionMode(QtGui.QAbstractSpinBox.CorrectToPreviousValue)
        self.timeFinalRecorrido.setObjectName(_fromUtf8("timeFinalRecorrido"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.timeFinalRecorrido)
        self.label_4 = QtGui.QLabel(self.groupBox)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_4)
        self.dateFechaRecorrido = QtGui.QDateEdit(self.groupBox)
        self.dateFechaRecorrido.setWrapping(False)
        self.dateFechaRecorrido.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.dateFechaRecorrido.setCorrectionMode(QtGui.QAbstractSpinBox.CorrectToPreviousValue)
        self.dateFechaRecorrido.setDateTime(QtCore.QDateTime(QtCore.QDate(2000, 1, 1), QtCore.QTime(0, 0, 0)))
        self.dateFechaRecorrido.setDate(QtCore.QDate(2000, 1, 1))
        self.dateFechaRecorrido.setCalendarPopup(True)
        self.dateFechaRecorrido.setObjectName(_fromUtf8("dateFechaRecorrido"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.dateFechaRecorrido)
        self.gridLayout_7.addWidget(self.groupBox, 0, 0, 1, 1)
        self.groupBox_4 = QtGui.QGroupBox(self.tabRecorrido)
        self.groupBox_4.setObjectName(_fromUtf8("groupBox_4"))
        self.verticalLayout_5 = QtGui.QVBoxLayout(self.groupBox_4)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.comboChoferRecorrido = QtGui.QComboBox(self.groupBox_4)
        self.comboChoferRecorrido.setEditable(True)
        self.comboChoferRecorrido.setObjectName(_fromUtf8("comboChoferRecorrido"))
        self.verticalLayout_5.addWidget(self.comboChoferRecorrido)
        self.gridLayout_7.addWidget(self.groupBox_4, 3, 0, 1, 1)
        self.pushConsultarRecorrido = QtGui.QPushButton(self.tabRecorrido)
        self.pushConsultarRecorrido.setObjectName(_fromUtf8("pushConsultarRecorrido"))
        self.gridLayout_7.addWidget(self.pushConsultarRecorrido, 4, 0, 1, 2)
        self.pushBorrarRecorrido = QtGui.QPushButton(self.tabRecorrido)
        self.pushBorrarRecorrido.setObjectName(_fromUtf8("pushBorrarRecorrido"))
        self.gridLayout_7.addWidget(self.pushBorrarRecorrido, 5, 0, 1, 2)
        self.groupBox_12 = QtGui.QGroupBox(self.tabRecorrido)
        self.groupBox_12.setObjectName(_fromUtf8("groupBox_12"))
        self.gridLayout_8 = QtGui.QGridLayout(self.groupBox_12)
        self.gridLayout_8.setObjectName(_fromUtf8("gridLayout_8"))
        self.label_19 = QtGui.QLabel(self.groupBox_12)
        self.label_19.setObjectName(_fromUtf8("label_19"))
        self.gridLayout_8.addWidget(self.label_19, 9, 0, 1, 1)
        self.lineLinea2 = QtGui.QLineEdit(self.groupBox_12)
        self.lineLinea2.setObjectName(_fromUtf8("lineLinea2"))
        self.gridLayout_8.addWidget(self.lineLinea2, 2, 3, 1, 1)
        self.label_22 = QtGui.QLabel(self.groupBox_12)
        self.label_22.setObjectName(_fromUtf8("label_22"))
        self.gridLayout_8.addWidget(self.label_22, 12, 0, 1, 1)
        self.lineFechaHoraRecorrido = QtGui.QLineEdit(self.groupBox_12)
        self.lineFechaHoraRecorrido.setMouseTracking(False)
        self.lineFechaHoraRecorrido.setAcceptDrops(False)
        self.lineFechaHoraRecorrido.setReadOnly(True)
        self.lineFechaHoraRecorrido.setObjectName(_fromUtf8("lineFechaHoraRecorrido"))
        self.gridLayout_8.addWidget(self.lineFechaHoraRecorrido, 0, 2, 1, 2)
        self.lineEdit_3 = QtGui.QLineEdit(self.groupBox_12)
        self.lineEdit_3.setMouseTracking(False)
        self.lineEdit_3.setAcceptDrops(False)
        self.lineEdit_3.setReadOnly(True)
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.gridLayout_8.addWidget(self.lineEdit_3, 10, 2, 1, 2)
        self.label_27 = QtGui.QLabel(self.groupBox_12)
        self.label_27.setObjectName(_fromUtf8("label_27"))
        self.gridLayout_8.addWidget(self.label_27, 3, 0, 1, 1)
        self.lineLinea1 = QtGui.QLineEdit(self.groupBox_12)
        self.lineLinea1.setMouseTracking(False)
        self.lineLinea1.setAcceptDrops(False)
        self.lineLinea1.setReadOnly(True)
        self.lineLinea1.setObjectName(_fromUtf8("lineLinea1"))
        self.gridLayout_8.addWidget(self.lineLinea1, 2, 2, 1, 1)
        self.label_17 = QtGui.QLabel(self.groupBox_12)
        self.label_17.setObjectName(_fromUtf8("label_17"))
        self.gridLayout_8.addWidget(self.label_17, 10, 0, 1, 1)
        self.lineEdit_5 = QtGui.QLineEdit(self.groupBox_12)
        self.lineEdit_5.setMouseTracking(False)
        self.lineEdit_5.setAcceptDrops(False)
        self.lineEdit_5.setReadOnly(True)
        self.lineEdit_5.setObjectName(_fromUtf8("lineEdit_5"))
        self.gridLayout_8.addWidget(self.lineEdit_5, 13, 2, 1, 2)
        self.label_25 = QtGui.QLabel(self.groupBox_12)
        self.label_25.setObjectName(_fromUtf8("label_25"))
        self.gridLayout_8.addWidget(self.label_25, 11, 0, 1, 1)
        self.label_23 = QtGui.QLabel(self.groupBox_12)
        self.label_23.setObjectName(_fromUtf8("label_23"))
        self.gridLayout_8.addWidget(self.label_23, 13, 0, 1, 1)
        self.label_18 = QtGui.QLabel(self.groupBox_12)
        self.label_18.setObjectName(_fromUtf8("label_18"))
        self.gridLayout_8.addWidget(self.label_18, 6, 0, 1, 1)
        self.lineEdit_2 = QtGui.QLineEdit(self.groupBox_12)
        self.lineEdit_2.setMouseTracking(False)
        self.lineEdit_2.setAcceptDrops(False)
        self.lineEdit_2.setReadOnly(True)
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.gridLayout_8.addWidget(self.lineEdit_2, 9, 2, 1, 2)
        self.lineEdit_4 = QtGui.QLineEdit(self.groupBox_12)
        self.lineEdit_4.setMouseTracking(False)
        self.lineEdit_4.setAcceptDrops(False)
        self.lineEdit_4.setReadOnly(True)
        self.lineEdit_4.setObjectName(_fromUtf8("lineEdit_4"))
        self.gridLayout_8.addWidget(self.lineEdit_4, 12, 2, 1, 2)
        self.label_20 = QtGui.QLabel(self.groupBox_12)
        self.label_20.setObjectName(_fromUtf8("label_20"))
        self.gridLayout_8.addWidget(self.label_20, 0, 0, 1, 1)
        self.lineEdit_7 = QtGui.QLineEdit(self.groupBox_12)
        self.lineEdit_7.setMouseTracking(False)
        self.lineEdit_7.setAcceptDrops(False)
        self.lineEdit_7.setReadOnly(True)
        self.lineEdit_7.setObjectName(_fromUtf8("lineEdit_7"))
        self.gridLayout_8.addWidget(self.lineEdit_7, 11, 2, 1, 2)
        self.lineChofer1 = QtGui.QLineEdit(self.groupBox_12)
        self.lineChofer1.setMouseTracking(False)
        self.lineChofer1.setAcceptDrops(False)
        self.lineChofer1.setReadOnly(True)
        self.lineChofer1.setObjectName(_fromUtf8("lineChofer1"))
        self.gridLayout_8.addWidget(self.lineChofer1, 3, 2, 1, 1)
        self.lineEdit = QtGui.QLineEdit(self.groupBox_12)
        self.lineEdit.setMouseTracking(False)
        self.lineEdit.setAcceptDrops(False)
        self.lineEdit.setReadOnly(True)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.gridLayout_8.addWidget(self.lineEdit, 6, 2, 1, 2)
        self.label_21 = QtGui.QLabel(self.groupBox_12)
        self.label_21.setObjectName(_fromUtf8("label_21"))
        self.gridLayout_8.addWidget(self.label_21, 2, 0, 1, 1)
        self.label_24 = QtGui.QLabel(self.groupBox_12)
        self.label_24.setObjectName(_fromUtf8("label_24"))
        self.gridLayout_8.addWidget(self.label_24, 14, 0, 1, 1)
        self.lineEdit_6 = QtGui.QLineEdit(self.groupBox_12)
        self.lineEdit_6.setMouseTracking(False)
        self.lineEdit_6.setAcceptDrops(False)
        self.lineEdit_6.setReadOnly(True)
        self.lineEdit_6.setObjectName(_fromUtf8("lineEdit_6"))
        self.gridLayout_8.addWidget(self.lineEdit_6, 14, 2, 1, 2)
        self.lineEdit_8 = QtGui.QLineEdit(self.groupBox_12)
        self.lineEdit_8.setMouseTracking(False)
        self.lineEdit_8.setAcceptDrops(False)
        self.lineEdit_8.setReadOnly(True)
        self.lineEdit_8.setObjectName(_fromUtf8("lineEdit_8"))
        self.gridLayout_8.addWidget(self.lineEdit_8, 15, 2, 1, 2)
        self.lineChofer2 = QtGui.QLineEdit(self.groupBox_12)
        self.lineChofer2.setObjectName(_fromUtf8("lineChofer2"))
        self.gridLayout_8.addWidget(self.lineChofer2, 3, 3, 1, 1)
        self.label_26 = QtGui.QLabel(self.groupBox_12)
        self.label_26.setObjectName(_fromUtf8("label_26"))
        self.gridLayout_8.addWidget(self.label_26, 15, 0, 1, 1)
        self.label_8 = QtGui.QLabel(self.groupBox_12)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.gridLayout_8.addWidget(self.label_8, 7, 0, 1, 1)
        self.lineEdit_9 = QtGui.QLineEdit(self.groupBox_12)
        self.lineEdit_9.setObjectName(_fromUtf8("lineEdit_9"))
        self.gridLayout_8.addWidget(self.lineEdit_9, 7, 2, 1, 2)
        self.gridLayout_7.addWidget(self.groupBox_12, 0, 1, 4, 1)
        self.tabWidget.addTab(self.tabRecorrido, _fromUtf8(""))
        self.tabParada = QtGui.QWidget()
        self.tabParada.setObjectName(_fromUtf8("tabParada"))
        self.gridLayout_10 = QtGui.QGridLayout(self.tabParada)
        self.gridLayout_10.setObjectName(_fromUtf8("gridLayout_10"))
        self.comboCocheParada = QtGui.QComboBox(self.tabParada)
        self.comboCocheParada.setEditable(True)
        self.comboCocheParada.setObjectName(_fromUtf8("comboCocheParada"))
        self.gridLayout_10.addWidget(self.comboCocheParada, 3, 2, 1, 4)
        self.line_3 = QtGui.QFrame(self.tabParada)
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.gridLayout_10.addWidget(self.line_3, 5, 1, 1, 5)
        self.label_16 = QtGui.QLabel(self.tabParada)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_16.sizePolicy().hasHeightForWidth())
        self.label_16.setSizePolicy(sizePolicy)
        self.label_16.setObjectName(_fromUtf8("label_16"))
        self.gridLayout_10.addWidget(self.label_16, 4, 1, 1, 1)
        self.label_39 = QtGui.QLabel(self.tabParada)
        self.label_39.setObjectName(_fromUtf8("label_39"))
        self.gridLayout_10.addWidget(self.label_39, 0, 1, 1, 1)
        self.tableParada = QtGui.QTableWidget(self.tabParada)
        self.tableParada.setObjectName(_fromUtf8("tableParada"))
        self.tableParada.setColumnCount(0)
        self.tableParada.setRowCount(0)
        self.tableParada.horizontalHeader().setCascadingSectionResizes(False)
        self.tableParada.horizontalHeader().setDefaultSectionSize(200)
        self.tableParada.horizontalHeader().setStretchLastSection(True)
        self.gridLayout_10.addWidget(self.tableParada, 9, 1, 1, 5)
        self.pushBorrarParada = QtGui.QPushButton(self.tabParada)
        self.pushBorrarParada.setObjectName(_fromUtf8("pushBorrarParada"))
        self.gridLayout_10.addWidget(self.pushBorrarParada, 8, 1, 1, 1)
        self.label_40 = QtGui.QLabel(self.tabParada)
        self.label_40.setObjectName(_fromUtf8("label_40"))
        self.gridLayout_10.addWidget(self.label_40, 3, 1, 1, 1)
        self.pushFiltrarParada = QtGui.QPushButton(self.tabParada)
        self.pushFiltrarParada.setObjectName(_fromUtf8("pushFiltrarParada"))
        self.gridLayout_10.addWidget(self.pushFiltrarParada, 6, 5, 1, 1)
        self.label_38 = QtGui.QLabel(self.tabParada)
        self.label_38.setObjectName(_fromUtf8("label_38"))
        self.gridLayout_10.addWidget(self.label_38, 6, 1, 1, 1)
        self.comboParadaParada = QtGui.QComboBox(self.tabParada)
        self.comboParadaParada.setEditable(True)
        self.comboParadaParada.setObjectName(_fromUtf8("comboParadaParada"))
        self.gridLayout_10.addWidget(self.comboParadaParada, 6, 2, 1, 3)
        self.dateFechaParada = QtGui.QDateEdit(self.tabParada)
        self.dateFechaParada.setWrapping(False)
        self.dateFechaParada.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.dateFechaParada.setCorrectionMode(QtGui.QAbstractSpinBox.CorrectToPreviousValue)
        self.dateFechaParada.setDateTime(QtCore.QDateTime(QtCore.QDate(2000, 1, 1), QtCore.QTime(0, 0, 0)))
        self.dateFechaParada.setDate(QtCore.QDate(2000, 1, 1))
        self.dateFechaParada.setCalendarPopup(True)
        self.dateFechaParada.setObjectName(_fromUtf8("dateFechaParada"))
        self.gridLayout_10.addWidget(self.dateFechaParada, 4, 2, 1, 4)
        self.pushConsultarParada = QtGui.QPushButton(self.tabParada)
        self.pushConsultarParada.setObjectName(_fromUtf8("pushConsultarParada"))
        self.gridLayout_10.addWidget(self.pushConsultarParada, 8, 2, 1, 4)
        self.label_9 = QtGui.QLabel(self.tabParada)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
        self.label_9.setSizePolicy(sizePolicy)
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.gridLayout_10.addWidget(self.label_9, 10, 1, 1, 1)
        self.comboChoferParada = QtGui.QComboBox(self.tabParada)
        self.comboChoferParada.setEditable(True)
        self.comboChoferParada.setObjectName(_fromUtf8("comboChoferParada"))
        self.gridLayout_10.addWidget(self.comboChoferParada, 0, 2, 1, 4)
        self.lineTiempoAcumuladoParada = QtGui.QLineEdit(self.tabParada)
        self.lineTiempoAcumuladoParada.setObjectName(_fromUtf8("lineTiempoAcumuladoParada"))
        self.gridLayout_10.addWidget(self.lineTiempoAcumuladoParada, 10, 2, 1, 4)
        self.tabWidget.addTab(self.tabParada, _fromUtf8(""))
        self.tabTarjeta = QtGui.QWidget()
        self.tabTarjeta.setObjectName(_fromUtf8("tabTarjeta"))
        self.gridLayout_2 = QtGui.QGridLayout(self.tabTarjeta)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.label_11 = QtGui.QLabel(self.tabTarjeta)
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.gridLayout_2.addWidget(self.label_11, 0, 0, 1, 1)
        self.tableTarjeta = QtGui.QTableWidget(self.tabTarjeta)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableTarjeta.sizePolicy().hasHeightForWidth())
        self.tableTarjeta.setSizePolicy(sizePolicy)
        self.tableTarjeta.setObjectName(_fromUtf8("tableTarjeta"))
        self.tableTarjeta.setColumnCount(0)
        self.tableTarjeta.setRowCount(0)
        self.tableTarjeta.horizontalHeader().setStretchLastSection(True)
        self.gridLayout_2.addWidget(self.tableTarjeta, 5, 0, 1, 3)
        self.pushBorrarTarjeta = QtGui.QPushButton(self.tabTarjeta)
        self.pushBorrarTarjeta.setObjectName(_fromUtf8("pushBorrarTarjeta"))
        self.gridLayout_2.addWidget(self.pushBorrarTarjeta, 4, 0, 1, 2)
        self.lineDocumentoTarjetas = QtGui.QLineEdit(self.tabTarjeta)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineDocumentoTarjetas.sizePolicy().hasHeightForWidth())
        self.lineDocumentoTarjetas.setSizePolicy(sizePolicy)
        self.lineDocumentoTarjetas.setObjectName(_fromUtf8("lineDocumentoTarjetas"))
        self.gridLayout_2.addWidget(self.lineDocumentoTarjetas, 0, 2, 1, 1)
        self.pushConsultarTarjeta = QtGui.QPushButton(self.tabTarjeta)
        self.pushConsultarTarjeta.setObjectName(_fromUtf8("pushConsultarTarjeta"))
        self.gridLayout_2.addWidget(self.pushConsultarTarjeta, 4, 2, 1, 1)
        self.comboComprobanteTarjetas = QtGui.QComboBox(self.tabTarjeta)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboComprobanteTarjetas.sizePolicy().hasHeightForWidth())
        self.comboComprobanteTarjetas.setSizePolicy(sizePolicy)
        self.comboComprobanteTarjetas.setEditable(True)
        self.comboComprobanteTarjetas.setObjectName(_fromUtf8("comboComprobanteTarjetas"))
        self.gridLayout_2.addWidget(self.comboComprobanteTarjetas, 1, 2, 1, 1)
        self.label_12 = QtGui.QLabel(self.tabTarjeta)
        self.label_12.setObjectName(_fromUtf8("label_12"))
        self.gridLayout_2.addWidget(self.label_12, 1, 0, 1, 1)
        self.tabWidget.addTab(self.tabTarjeta, _fromUtf8(""))
        self.tabCoche = QtGui.QWidget()
        self.tabCoche.setObjectName(_fromUtf8("tabCoche"))
        self.gridLayout_6 = QtGui.QGridLayout(self.tabCoche)
        self.gridLayout_6.setObjectName(_fromUtf8("gridLayout_6"))
        self.tableWidget_2 = QtGui.QTableWidget(self.tabCoche)
        self.tableWidget_2.setObjectName(_fromUtf8("tableWidget_2"))
        self.tableWidget_2.setColumnCount(0)
        self.tableWidget_2.setRowCount(0)
        self.tableWidget_2.horizontalHeader().setStretchLastSection(True)
        self.gridLayout_6.addWidget(self.tableWidget_2, 4, 0, 1, 2)
        self.label_13 = QtGui.QLabel(self.tabCoche)
        self.label_13.setObjectName(_fromUtf8("label_13"))
        self.gridLayout_6.addWidget(self.label_13, 1, 0, 1, 1)
        self.pushBorrarCoche = QtGui.QPushButton(self.tabCoche)
        self.pushBorrarCoche.setObjectName(_fromUtf8("pushBorrarCoche"))
        self.gridLayout_6.addWidget(self.pushBorrarCoche, 3, 0, 1, 1)
        self.comboSerieCoches = QtGui.QComboBox(self.tabCoche)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboSerieCoches.sizePolicy().hasHeightForWidth())
        self.comboSerieCoches.setSizePolicy(sizePolicy)
        self.comboSerieCoches.setEditable(True)
        self.comboSerieCoches.setObjectName(_fromUtf8("comboSerieCoches"))
        self.gridLayout_6.addWidget(self.comboSerieCoches, 1, 1, 1, 1)
        self.pushConsultarCoche = QtGui.QPushButton(self.tabCoche)
        self.pushConsultarCoche.setObjectName(_fromUtf8("pushConsultarCoche"))
        self.gridLayout_6.addWidget(self.pushConsultarCoche, 3, 1, 1, 1)
        self.label_14 = QtGui.QLabel(self.tabCoche)
        self.label_14.setObjectName(_fromUtf8("label_14"))
        self.gridLayout_6.addWidget(self.label_14, 2, 0, 1, 1)
        self.comboCocheCoches = QtGui.QComboBox(self.tabCoche)
        self.comboCocheCoches.setEditable(True)
        self.comboCocheCoches.setObjectName(_fromUtf8("comboCocheCoches"))
        self.gridLayout_6.addWidget(self.comboCocheCoches, 2, 1, 1, 1)
        self.tabWidget.addTab(self.tabCoche, _fromUtf8(""))
        self.verticalLayout.addWidget(self.tabWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 937, 28))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menu_Archivo = QtGui.QMenu(self.menubar)
        self.menu_Archivo.setObjectName(_fromUtf8("menu_Archivo"))
        self.actionAcercaDe = QtGui.QMenu(self.menubar)
        self.actionAcercaDe.setObjectName(_fromUtf8("actionAcercaDe"))
        self.menuConfiguraci_n = QtGui.QMenu(self.menubar)
        self.menuConfiguraci_n.setObjectName(_fromUtf8("menuConfiguraci_n"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionSalir = QtGui.QAction(MainWindow)
        self.actionSalir.setObjectName(_fromUtf8("actionSalir"))
        self.actionAcerca_de = QtGui.QAction(MainWindow)
        self.actionAcerca_de.setObjectName(_fromUtf8("actionAcerca_de"))
        self.actionVersi_n = QtGui.QAction(MainWindow)
        self.actionVersi_n.setObjectName(_fromUtf8("actionVersi_n"))
        self.actionExportar = QtGui.QAction(MainWindow)
        self.actionExportar.setObjectName(_fromUtf8("actionExportar"))
        self.actionConfiguracion = QtGui.QAction(MainWindow)
        self.actionConfiguracion.setObjectName(_fromUtf8("actionConfiguracion"))
        self.actionModoDesarrollador = QtGui.QAction(MainWindow)
        self.actionModoDesarrollador.setObjectName(_fromUtf8("actionModoDesarrollador"))
        self.menu_Archivo.addAction(self.actionExportar)
        self.menu_Archivo.addSeparator()
        self.menu_Archivo.addAction(self.actionSalir)
        self.actionAcercaDe.addAction(self.actionAcerca_de)
        self.menuConfiguraci_n.addAction(self.actionConfiguracion)
        self.menubar.addAction(self.menu_Archivo.menuAction())
        self.menubar.addAction(self.menuConfiguraci_n.menuAction())
        self.menubar.addAction(self.actionAcercaDe.menuAction())

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.tabWidget, self.timeInicioRecorrido)
        MainWindow.setTabOrder(self.timeInicioRecorrido, self.timeFinalRecorrido)
        MainWindow.setTabOrder(self.timeFinalRecorrido, self.dateFechaRecorrido)
        MainWindow.setTabOrder(self.dateFechaRecorrido, self.spinDuracionTotalRecorrido)
        MainWindow.setTabOrder(self.spinDuracionTotalRecorrido, self.spinTiempoAntesRecorrido)
        MainWindow.setTabOrder(self.spinTiempoAntesRecorrido, self.spinTiempoDespuesRecorrido)
        MainWindow.setTabOrder(self.spinTiempoDespuesRecorrido, self.comboChoferRecorrido)
        MainWindow.setTabOrder(self.comboChoferRecorrido, self.pushConsultarRecorrido)
        MainWindow.setTabOrder(self.pushConsultarRecorrido, self.pushBorrarRecorrido)
        MainWindow.setTabOrder(self.pushBorrarRecorrido, self.lineFechaHoraRecorrido)
        MainWindow.setTabOrder(self.lineFechaHoraRecorrido, self.lineLinea1)
        MainWindow.setTabOrder(self.lineLinea1, self.lineLinea2)
        MainWindow.setTabOrder(self.lineLinea2, self.lineChofer1)
        MainWindow.setTabOrder(self.lineChofer1, self.lineChofer2)
        MainWindow.setTabOrder(self.lineChofer2, self.lineEdit)
        MainWindow.setTabOrder(self.lineEdit, self.lineEdit_9)
        MainWindow.setTabOrder(self.lineEdit_9, self.lineEdit_2)
        MainWindow.setTabOrder(self.lineEdit_2, self.lineEdit_3)
        MainWindow.setTabOrder(self.lineEdit_3, self.lineEdit_7)
        MainWindow.setTabOrder(self.lineEdit_7, self.lineEdit_4)
        MainWindow.setTabOrder(self.lineEdit_4, self.lineEdit_5)
        MainWindow.setTabOrder(self.lineEdit_5, self.lineEdit_6)
        MainWindow.setTabOrder(self.lineEdit_6, self.lineEdit_8)
        MainWindow.setTabOrder(self.lineEdit_8, self.comboChoferParada)
        MainWindow.setTabOrder(self.comboChoferParada, self.comboCocheParada)
        MainWindow.setTabOrder(self.comboCocheParada, self.dateFechaParada)
        MainWindow.setTabOrder(self.dateFechaParada, self.pushConsultarParada)
        MainWindow.setTabOrder(self.pushConsultarParada, self.pushBorrarParada)
        MainWindow.setTabOrder(self.pushBorrarParada, self.comboParadaParada)
        MainWindow.setTabOrder(self.comboParadaParada, self.pushFiltrarParada)
        MainWindow.setTabOrder(self.pushFiltrarParada, self.tableParada)
        MainWindow.setTabOrder(self.tableParada, self.lineDocumentoTarjetas)
        MainWindow.setTabOrder(self.lineDocumentoTarjetas, self.comboComprobanteTarjetas)
        MainWindow.setTabOrder(self.comboComprobanteTarjetas, self.pushConsultarTarjeta)
        MainWindow.setTabOrder(self.pushConsultarTarjeta, self.pushBorrarTarjeta)
        MainWindow.setTabOrder(self.pushBorrarTarjeta, self.tableTarjeta)
        MainWindow.setTabOrder(self.tableTarjeta, self.comboSerieCoches)
        MainWindow.setTabOrder(self.comboSerieCoches, self.comboCocheCoches)
        MainWindow.setTabOrder(self.comboCocheCoches, self.pushConsultarCoche)
        MainWindow.setTabOrder(self.pushConsultarCoche, self.pushBorrarCoche)
        MainWindow.setTabOrder(self.pushBorrarCoche, self.tableWidget_2)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Estadísticas y controles de tiempo - TransBus", None))
        self.groupBox_5.setTitle(_translate("MainWindow", "Análisis", None))
        self.label_3.setText(_translate("MainWindow", "minutos de duración total de la vuelta", None))
        self.groupBox_3.setTitle(_translate("MainWindow", "Tolerancias", None))
        self.label.setText(_translate("MainWindow", "minutos antes en terminal", None))
        self.label_2.setText(_translate("MainWindow", "minutos después en terminal", None))
        self.groupBox.setTitle(_translate("MainWindow", "Fecha y rango horario", None))
        self.label_5.setText(_translate("MainWindow", "Inicio:", None))
        self.timeInicioRecorrido.setDisplayFormat(_translate("MainWindow", "hh:mm:ss", None))
        self.label_6.setText(_translate("MainWindow", "Final: ", None))
        self.timeFinalRecorrido.setDisplayFormat(_translate("MainWindow", "hh:mm:ss", None))
        self.label_4.setText(_translate("MainWindow", "Fecha:", None))
        self.dateFechaRecorrido.setDisplayFormat(_translate("MainWindow", "dd/MM/yy", None))
        self.groupBox_4.setTitle(_translate("MainWindow", "Chofer", None))
        self.pushConsultarRecorrido.setText(_translate("MainWindow", "Consultar", None))
        self.pushBorrarRecorrido.setText(_translate("MainWindow", "Borrar consulta", None))
        self.groupBox_12.setTitle(_translate("MainWindow", "Resultado de la consulta", None))
        self.label_19.setText(_translate("MainWindow", "Duración de la vuelta mas lenta:", None))
        self.label_22.setText(_translate("MainWindow", "Tiempo de circulación total:", None))
        self.label_27.setText(_translate("MainWindow", "Chofer:", None))
        self.label_17.setText(_translate("MainWindow", "Duración de la vuelta promedio:", None))
        self.label_25.setText(_translate("MainWindow", "Número de vueltas:", None))
        self.label_23.setText(_translate("MainWindow", "Tiempo detenido total:", None))
        self.label_18.setText(_translate("MainWindow", "Duración de la vuelta mas rápida:", None))
        self.label_20.setText(_translate("MainWindow", "Fecha y hora:", None))
        self.label_21.setText(_translate("MainWindow", "Linea:", None))
        self.label_24.setText(_translate("MainWindow", "Parada con mas tiempo detenido:", None))
        self.label_26.setText(_translate("MainWindow", "Tiempo de circulación neto:", None))
        self.label_8.setText(_translate("MainWindow", "Lugar de inicio:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabRecorrido), _translate("MainWindow", "Consultas de tiempo de recorrido", None))
        self.label_16.setText(_translate("MainWindow", "Fecha:", None))
        self.label_39.setText(_translate("MainWindow", "Chofer:", None))
        self.tableParada.setSortingEnabled(True)
        self.pushBorrarParada.setText(_translate("MainWindow", "Borrar consulta", None))
        self.label_40.setText(_translate("MainWindow", "Coche:", None))
        self.pushFiltrarParada.setText(_translate("MainWindow", "Filtrar", None))
        self.label_38.setText(_translate("MainWindow", "Parada terminal:", None))
        self.dateFechaParada.setDisplayFormat(_translate("MainWindow", "dd/MM/yy", None))
        self.pushConsultarParada.setText(_translate("MainWindow", "Consultar", None))
        self.label_9.setText(_translate("MainWindow", "Tiempo acum.:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabParada), _translate("MainWindow", "Consulta de tiempo en parada", None))
        self.label_11.setText(_translate("MainWindow", "Documento:", None))
        self.tableTarjeta.setSortingEnabled(True)
        self.pushBorrarTarjeta.setText(_translate("MainWindow", "Borrar consulta", None))
        self.pushConsultarTarjeta.setText(_translate("MainWindow", "Consultar", None))
        self.label_12.setText(_translate("MainWindow", "Comprobante:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabTarjeta), _translate("MainWindow", "Tarjetas", None))
        self.label_13.setText(_translate("MainWindow", "Serie:", None))
        self.pushBorrarCoche.setText(_translate("MainWindow", "Borrar consulta", None))
        self.pushConsultarCoche.setText(_translate("MainWindow", "Consultar", None))
        self.label_14.setText(_translate("MainWindow", "Coche:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabCoche), _translate("MainWindow", "Coches", None))
        self.menu_Archivo.setTitle(_translate("MainWindow", "&Archivo", None))
        self.actionAcercaDe.setTitle(_translate("MainWindow", "A&yuda", None))
        self.menuConfiguraci_n.setTitle(_translate("MainWindow", "Editar", None))
        self.actionSalir.setText(_translate("MainWindow", "Salir", None))
        self.actionAcerca_de.setText(_translate("MainWindow", "Acerca de...", None))
        self.actionVersi_n.setText(_translate("MainWindow", "Versión", None))
        self.actionExportar.setText(_translate("MainWindow", "Exportar", None))
        self.actionConfiguracion.setText(_translate("MainWindow", "Configuración", None))
        self.actionModoDesarrollador.setText(_translate("MainWindow", "Modo desarrollador", None))

