# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'AcercaDe.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_AcercaDe(object):
    def setupUi(self, AcercaDe):
        AcercaDe.setObjectName(_fromUtf8("AcercaDe"))
        AcercaDe.resize(268, 214)
        AcercaDe.setMaximumSize(QtCore.QSize(999999, 99999))
        self.verticalLayout = QtGui.QVBoxLayout(AcercaDe)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_2 = QtGui.QLabel(AcercaDe)
        font = QtGui.QFont()
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        self.label_2.setFont(font)
        self.label_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.label_2.setTextFormat(QtCore.Qt.AutoText)
        self.label_2.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout.addWidget(self.label_2)
        self.label = QtGui.QLabel(AcercaDe)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.label_3 = QtGui.QLabel(AcercaDe)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout.addWidget(self.label_3)
        self.label_5 = QtGui.QLabel(AcercaDe)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.verticalLayout.addWidget(self.label_5)
        self.label_4 = QtGui.QLabel(AcercaDe)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout.addWidget(self.label_4)
        self.listWidget = QtGui.QListWidget(AcercaDe)
        self.listWidget.setEnabled(True)
        self.listWidget.setFrameShape(QtGui.QFrame.NoFrame)
        self.listWidget.setFrameShadow(QtGui.QFrame.Plain)
        self.listWidget.setLineWidth(0)
        self.listWidget.setAutoScroll(False)
        self.listWidget.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.listWidget.setProperty("showDropIndicator", False)
        self.listWidget.setObjectName(_fromUtf8("listWidget"))
        item = QtGui.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtGui.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtGui.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtGui.QListWidgetItem()
        self.listWidget.addItem(item)
        self.verticalLayout.addWidget(self.listWidget)

        self.retranslateUi(AcercaDe)
        QtCore.QMetaObject.connectSlotsByName(AcercaDe)

    def retranslateUi(self, AcercaDe):
        AcercaDe.setWindowTitle(_translate("AcercaDe", "Acerca de...", None))
        self.label_2.setText(_translate("AcercaDe", "Programa de control y estadísticas", None))
        self.label.setText(_translate("AcercaDe", "TransBus S.R.L. - La Estrella S.R.L.", None))
        self.label_3.setText(_translate("AcercaDe", "Version 0.1", None))
        self.label_5.setText(_translate("AcercaDe", "Año 2015", None))
        self.label_4.setText(_translate("AcercaDe", "Programas y librerías utilizados:", None))
        __sortingEnabled = self.listWidget.isSortingEnabled()
        self.listWidget.setSortingEnabled(False)
        item = self.listWidget.item(0)
        item.setText(_translate("AcercaDe", "Python 3", None))
        item = self.listWidget.item(1)
        item.setText(_translate("AcercaDe", "PyQt 4", None))
        item = self.listWidget.item(2)
        item.setText(_translate("AcercaDe", "xlwt", None))
        item = self.listWidget.item(3)
        item.setText(_translate("AcercaDe", "PyInstaller", None))
        self.listWidget.setSortingEnabled(__sortingEnabled)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    AcercaDe = QtGui.QWidget()
    ui = Ui_AcercaDe()
    ui.setupUi(AcercaDe)
    AcercaDe.show()
    sys.exit(app.exec_())

