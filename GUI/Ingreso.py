# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GUI/Ingreso.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Ingreso(object):
    def setupUi(self, Ingreso):
        Ingreso.setObjectName(_fromUtf8("Ingreso"))
        Ingreso.resize(298, 137)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Ingreso.sizePolicy().hasHeightForWidth())
        Ingreso.setSizePolicy(sizePolicy)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../icono.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Ingreso.setWindowIcon(icon)
        self.gridLayout = QtGui.QGridLayout(Ingreso)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(Ingreso)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.linePassword = QtGui.QLineEdit(Ingreso)
        self.linePassword.setEchoMode(QtGui.QLineEdit.Password)
        self.linePassword.setObjectName(_fromUtf8("linePassword"))
        self.gridLayout.addWidget(self.linePassword, 1, 1, 1, 1)
        self.lineUsuario = QtGui.QLineEdit(Ingreso)
        self.lineUsuario.setObjectName(_fromUtf8("lineUsuario"))
        self.gridLayout.addWidget(self.lineUsuario, 0, 1, 1, 1)
        self.label_2 = QtGui.QLabel(Ingreso)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.pushIngresar = QtGui.QPushButton(Ingreso)
        self.pushIngresar.setObjectName(_fromUtf8("pushIngresar"))
        self.gridLayout.addWidget(self.pushIngresar, 2, 0, 1, 2)
        self.labelEstado = QtGui.QLabel(Ingreso)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelEstado.sizePolicy().hasHeightForWidth())
        self.labelEstado.setSizePolicy(sizePolicy)
        self.labelEstado.setText(_fromUtf8(""))
        self.labelEstado.setObjectName(_fromUtf8("labelEstado"))
        self.gridLayout.addWidget(self.labelEstado, 3, 0, 1, 2)

        self.retranslateUi(Ingreso)
        QtCore.QMetaObject.connectSlotsByName(Ingreso)
        Ingreso.setTabOrder(self.lineUsuario, self.linePassword)
        Ingreso.setTabOrder(self.linePassword, self.pushIngresar)

    def retranslateUi(self, Ingreso):
        Ingreso.setWindowTitle(_translate("Ingreso", "Ingreso al sistema", None))
        self.label.setText(_translate("Ingreso", "Nombre de usuario:", None))
        self.label_2.setText(_translate("Ingreso", "Contraseña:", None))
        self.pushIngresar.setText(_translate("Ingreso", "Ingresar", None))

