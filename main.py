#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Consulta la base de datos de SiscaDat para obtener sevicios basicos."""

import sys
import os

from GUI.AcercaDe import Ui_AcercaDe
from GUI.Configuracion import Ui_Configuracion
from GUI.Ingreso import Ui_Ingreso
from GUI.Principal import Ui_MainWindow
from PyQt4.QtCore import QDate, QTime
from PyQt4.QtGui import QApplication, QDialog, QFileDialog, QHeaderView, QIcon, QMainWindow, QMessageBox, QPixmap, QTableWidgetItem, QWidget

from datetime import datetime
from datetime import timedelta
import re
import time
import urllib.request
import urllib.parse
import xlwt

GUI = os.path.join(os.getcwd(), 'GUI')
icons = os.path.join(GUI, 'icons')

# MUESTRA EL ICONO CORRECTAMENTE EN LA BARRA DE HERRAMIENTAS DE WINDOWS
if os.name == 'nt':
    import ctypes
    myappid = 'TransBus - Estadistica y Control' # arbitrary string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)


class AcercaDe(QDialog):
    """Clase para la ventana de informacion del programa."""
    def __init__(self, parent=None):
        super(AcercaDe, self).__init__()
        QWidget.__init__(self, parent)
        self.ui = Ui_AcercaDe()
        self.ui.setupUi(self)

        self.setWindowIcon(QIcon(os.path.join(icons, 'icono.png')))

class Configuracion(QDialog):
    """Clase para la ventana de configuracion de la aplicacion."""
    def __init__(self, parent=None):
        super(Configuracion, self).__init__()
        QWidget.__init__(self, parent)
        self.ui = Ui_Configuracion()
        self.ui.setupUi(self)

        self.setWindowIcon(QIcon(os.path.join(icons, 'icono.png')))

        self.ui.radioPrincipal.clicked.connect(self.configurarServidor)
        self.ui.radioSecundario.clicked.connect(self.configurarServidor)

        if servidorActual == servidorPrincipal:
            self.ui.radioPrincipal.setChecked(True)
        elif servidorActual == servidorSecundario:
            self.ui.radioSecundario.setChecked(True)

    def configurarServidor(self):
        global servidorActual

        if self.ui.radioPrincipal.isChecked():
            servidorActual = servidorPrincipal
        elif self.ui.radioSecundario.isChecked():
            servidorActual = servidorSecundario
        return servidorActual


class Ingreso(QDialog):
    """Clase para la ventana de ingreso para la aplicacion."""
    def __init__(self, parent=None):
        super(Ingreso, self).__init__()
        self.ui = Ui_Ingreso()
        self.ui.setupUi(self)

        self.setWindowIcon(QIcon(os.path.join(icons, 'icono.png')))

        self.ui.pushIngresar.clicked.connect(self.verificarUsuario)

    def verificarUsuario(self):
        global usuario
        global password
        global key

        usuario = self.ui.lineUsuario.text()
        password = self.ui.linePassword.text()

        respData = query(method="get_key", usuario=usuario, password=password)

        if re.findall(r'errorcode="1040"', str(respData)):
            self.ui.labelEstado.setText("Error de usuario o contraseña")
        elif re.findall(r'OK', str(respData)):
            key = re.findall(r'key="([a-z0-9]*)"', str(respData))
            key = str(key[0])
            self.accept()
            return key
        else:
            print("Error desconocido, verifique su conexion y sus datos.")


class MainWindow(QMainWindow):
    """Clase para la ventana principal de la aplicacion."""

    def __init__(self, parent=None):
        """Inicializacion ventana principal y conexiones de funciones."""
        super(MainWindow, self).__init__(parent)
        QWidget.__init__(self, parent)

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.setWindowIcon(QIcon(os.path.join(icons, 'icono.png')))

        self.ui.actionSalir.triggered.connect(self.cerrarAplicacion)
        self.ui.actionExportar.triggered.connect(self.Exportar)
        self.ui.actionConfiguracion.triggered.connect(self.Configuracion)
        self.ui.actionAcerca_de.triggered.connect(self.AcercaDe)

        self.ui.pushConsultarCoche.clicked.connect(self.queryCoche)
        self.ui.pushBorrarCoche.clicked.connect(self.borrarCoche)
        self.ui.pushConsultarParada.clicked.connect(self.queryParada)
        self.ui.pushFiltrarParada.clicked.connect(self.filtrarParada)
        self.ui.pushBorrarParada.clicked.connect(self.borrarParada)
        self.ui.pushConsultarRecorrido.clicked.connect(self.queryRecorrido)
        self.ui.pushBorrarRecorrido.clicked.connect(self.borrarRecorrido)
        self.ui.pushConsultarTarjeta.clicked.connect(self.queryTarjeta)
        self.ui.pushBorrarTarjeta.clicked.connect(self.borrarTarjeta)

        self.ui.dateFechaRecorrido.setDate(QDate.currentDate())
        self.ui.dateFechaParada.setDate(QDate.currentDate())

        self.ui.timeInicioRecorrido.setTime(QTime(6, 0, 0))
        self.ui.timeFinalRecorrido.setTime(QTime(6, 0, 0))

        self.ui.lineTiempoAcumuladoParada.setText('0:00:00')

        self.ui.tableParada.horizontalHeader().setResizeMode(QHeaderView.Stretch)
        headerParada = ['Código de servicio', 'Parada', 'Horario teórico', 'Entrada a terminal', 'Salida de terminal', 'Tiempo parcial en terminal']
        self.ui.tableParada.setColumnCount(len(headerParada))
        self.ui.tableParada.setHorizontalHeaderLabels(headerParada)

        self.ui.tableTarjeta.horizontalHeader().setResizeMode(QHeaderView.Stretch)
        headerTarjeta = ['DNI', 'Vencimiento', 'Alta fecha', 'Alta por', 'Comprobante', 'Serie tarjeta']
        self.ui.tableTarjeta.setColumnCount(len(headerTarjeta))
        self.ui.tableTarjeta.setHorizontalHeaderLabels(headerTarjeta)

        self.queryComprobante()
        self.queryPersonal()
        self.queryVehiculos()
        self.queryLocalidad()

    def AcercaDe(self):
        """Llamada a la ventana de informacion del programa."""
        self.w = AcercaDe()
        self.w.exec_()

    def cerrarAplicacion(self):
        """Consulta de cierre limpio de la aplicacion."""
        opcion = QMessageBox.question(self, "Cerrar la aplicacion", "Esta seguro que desea salir de la aplicacion?", \
                    QMessageBox.Yes | QMessageBox.No)
        if opcion == QMessageBox.Yes:
            sys.exit()

    def Configuracion(self):
        """Llamada a la ventana de configuracion."""
        self.w = Configuracion()
        self.w.exec_()

    def Exportar(self):
        """Exportación de la búsqueda en archivo xls."""

        tiempoInicio = []
        tiempoFin = []

        for tiempo in range(len(tiempoInicioTerminal)):
            tiempoInicio.append(tiempoInicioTerminal[tiempo].time())
            tiempoFin.append(tiempoFinTerminal[tiempo].time())

        header = ['Código de servicio', 'Parada', 'Horario teórico', 'Entrada a terminal', 'Salida de terminal',
                  'Tiempo parcial en terminal']
        tiempos = [codigoDeServicio, terminalDeOrigen, tiempoTeoricoTerminal, tiempoInicio,
                   tiempoFin, tiempoEnTerminal]

        wb = xlwt.Workbook()
        ws = wb.add_sheet("%s" % dataPersonalID[chofer])

        ws.write(0, 0, header[0])
        ws.write(0, 1, header[1])
        ws.write(0, 2, header[2])
        ws.write(0, 3, header[3])
        ws.write(0, 4, header[4])
        ws.write(0, 5, header[5])

        for col in range(len(tiempos)):
            for row in range(len(tiempos[0])):
                ws.write(row + 1, col, str(tiempos[col][row]))

        ws.write(len(tiempoInicio) + 2, 4, "Tiempo acumulado")
        ws.write(len(tiempoInicio) + 2, 5, str(tiempoAcumuladoEnTerminal))

        ws.col(0).width = 256 * (len(header[0]) + 1)
        ws.col(1).width = 256 * (max([len(parada) for parada in tiempos[1]]) + 5)
        ws.col(2).width = 256 * (len(header[2]) + 1)
        ws.col(3).width = 256 * (len(header[3]) + 1)
        ws.col(4).width = 256 * (len(header[4]) + 1)
        ws.col(5).width = 256 * (len(header[5]) + 1)

        wb.save("Analisis de %s, %s.xls" % (dataPersonalID[chofer],
                                            self.ui.dateFechaParada.date().toString("dd-MM-yyyy")))

    def queryCoche(self):
        pass

    def borrarCoche(self):
        self.ui.comboSerieCoches.setCurrentIndex(0)
        self.ui.comboCocheCoches.setCurrentIndex(0)

    def queryComprobante(self):
        """Solicitud de los Comprobantes configurados en el sistema."""
        global dataComprobantes

        respData = query(method="comprobante")

        dataComprobantes = re.findall(r'<data nombre="(.*?)".*?codigo="(.*?)" />', str(respData))
        dataComprobantes = dict(dataComprobantes)

        self.ui.comboComprobanteTarjetas.addItem('Ninguno')
        for comprobante in sorted(dataComprobantes.keys(), key=str.lower):
            self.ui.comboComprobanteTarjetas.addItem(comprobante)

    def queryLocalidad(self):
        """Solicitud de Localidades en el sistema."""
        global dataLocalidadID
        global dataLocalidadNombre

        respData = query(method="localidad")

        dataLocalidadNombre = re.findall(r'<data.*?nombre="(.*?)".*?codigo="(.*?)".*?/>', str(respData))
        dataLocalidadNombre = dict(dataLocalidadNombre)

        dataLocalidadID = {v: k for k, v in dataLocalidadNombre.items()}

        for data in sorted(dataLocalidadNombre.keys(), key=str.lower):
            self.ui.comboParadaParada.addItem(data)

    def queryParada(self):
        """Solicitud de tiempo detenido en terminal."""
        global chofer
        global terminalDeOrigen
        global codigoDeServicio
        global terminalDeOrigen
        global tiempoTeoricoTerminal
        global tiempoInicioTerminal
        global tiempoFinTerminal
        global tiempoEnTerminal
        global tiempoAcumuladoEnTerminal

        chofer = dataPersonalNombre[self.ui.comboChoferParada.currentText()]
        coche = dataVehiculos[self.ui.comboCocheParada.currentText()]
        servicioFecha = self.ui.dateFechaParada.date().toString("yyyy-MM-dd")

        codigoDeServicio = []
        terminalDeOrigen = []
        tiempoTeoricoTerminal = []
        tiempoInicioTerminal = []
        tiempoFinTerminal = []
        tiempoEnTerminal = []

        cantidadFilas = 0
        k = 0

        if chofer != 'Ninguno' and coche == 'Ninguno':  # Consulta por Chofer
            respData = query(method="transaccion", servicioFecha=servicioFecha, chofer=chofer)
        elif chofer == 'Ninguno' and coche != 'Ninguno':  # Consulta por Coche
            respData = query(method="transaccion", servicioFecha=servicioFecha, coche=coche)
        else:  # Consulta por todas las condiciones
            respData = query(method="transaccion", servicioFecha=servicioFecha, chofer=chofer, coche=coche)

        if "affected=\"0\"" in str(respData):
            QMessageBox.about(self, "Información", "No hay información que cumpla con el criterio de búsqueda")
            self.ui.tableParada.setRowCount(0);
            self.ui.lineTiempoAcumuladoParada.setText('0:00:00')
        else:
            dataParada = re.findall(r'<data.*?fecha="(.*?)".*?origen="(.*?)".*?servicio="(.*?)".*?servicio_hora="(.*?)"'
                                    r'.*?comprobante="(.*?)".*?/>', str(respData))

            tiempoAcumuladoEnTerminal = timedelta()

            for parada in dataParada:
                if parada[4] == "80" or parada[4] == "86" or parada[4] == "93":
                    tiempoInicioTerminal.append(datetime.strptime(parada[0], '%Y-%m-%d %H:%M:%S'))
                    tiempoTeoricoTerminal.append(parada[3])
                    codigoDeServicio.append(parada[2])
                    terminalDeOrigen.append(dataLocalidadID[str(parada[1])])
                    for parada in dataParada:
                        if (parada[2] == "81" or parada[4] == "85" or parada[4] == "92") and \
                                        datetime.strptime(parada[0], '%Y-%m-%d %H:%M:%S') >= tiempoInicioTerminal[k]:
                            tiempoFinTerminal.append(datetime.strptime(parada[0], '%Y-%m-%d %H:%M:%S'))
                            tiempoEnTerminal.append(tiempoFinTerminal[k] - tiempoInicioTerminal[k])
                            tiempoAcumuladoEnTerminal += tiempoEnTerminal[k]
                            cantidadFilas += 1
                            break
                        else:
                            continue
                    k += 1

            header = ['Código de servicio', 'Parada', 'Horario teórico', 'Entrada a terminal', 'Salida de terminal',
                      'Tiempo parcial en terminal']
            self.ui.tableParada.setRowCount(cantidadFilas)
            self.ui.tableParada.setColumnCount(len(header))
            self.ui.tableParada.setHorizontalHeaderLabels(header)

            for fila in range(cantidadFilas):
                self.ui.tableParada.setItem(fila, 0, QTableWidgetItem(codigoDeServicio[fila]))
                self.ui.tableParada.setItem(fila, 1, QTableWidgetItem(terminalDeOrigen[fila]))
                self.ui.tableParada.setItem(fila, 2, QTableWidgetItem(tiempoTeoricoTerminal[fila]))
                self.ui.tableParada.setItem(fila, 3, QTableWidgetItem(str(tiempoInicioTerminal[fila].time())))
                self.ui.tableParada.setItem(fila, 4, QTableWidgetItem(str(tiempoFinTerminal[fila].time())))
                self.ui.tableParada.setItem(fila, 5, QTableWidgetItem(str(tiempoEnTerminal[fila])))

            self.ui.lineTiempoAcumuladoParada.setText(str(tiempoAcumuladoEnTerminal))

    def filtrarParada(self):
        """Filtrado de la información obtenida en queryParada."""
        codigoDeServicioFiltrado = []
        terminalDeOrigenFiltrado = []
        tiempoTeoricoTerminalFiltrado = []
        tiempoInicioTerminalFiltrado = []
        tiempoFinTerminalFiltrado = []
        tiempoEnTerminalFiltrado = []
        tiempoAcumuladoEnTerminal = timedelta()
        cantidadFilas = 0
        i = 0

        for terminal in terminalDeOrigen:
            if terminal == self.ui.comboParadaParada.currentText():
                tiempoInicioTerminalFiltrado.append(tiempoInicioTerminal[i])
                tiempoTeoricoTerminalFiltrado.append(tiempoTeoricoTerminal[i])
                codigoDeServicioFiltrado.append(codigoDeServicio[i])
                terminalDeOrigenFiltrado.append(terminal)
                tiempoFinTerminalFiltrado.append(tiempoFinTerminal[i])
                tiempoEnTerminalFiltrado.append(tiempoEnTerminal[i])
                tiempoAcumuladoEnTerminal += tiempoEnTerminal[i]
                cantidadFilas += 1
            i += 1

        header = ['Código de servicio', 'Parada', 'Horario teórico', 'Entrada a terminal', 'Salida de terminal',
                  'Tiempo parcial en terminal']
        self.ui.tableParada.setRowCount(cantidadFilas)
        self.ui.tableParada.setColumnCount(len(header))
        self.ui.tableParada.setHorizontalHeaderLabels(header)

        for fila in range(cantidadFilas):
            self.ui.tableParada.setItem(fila, 0, QTableWidgetItem(codigoDeServicioFiltrado[fila]))
            self.ui.tableParada.setItem(fila, 1, QTableWidgetItem(terminalDeOrigenFiltrado[fila]))
            self.ui.tableParada.setItem(fila, 2, QTableWidgetItem(tiempoTeoricoTerminalFiltrado[fila]))
            self.ui.tableParada.setItem(fila, 3, QTableWidgetItem(str(tiempoInicioTerminalFiltrado[fila].time())))
            self.ui.tableParada.setItem(fila, 4, QTableWidgetItem(str(tiempoFinTerminalFiltrado[fila].time())))
            self.ui.tableParada.setItem(fila, 5, QTableWidgetItem(str(tiempoEnTerminalFiltrado[fila])))

        if len(terminal) == 0:
            self.ui.lineTiempoAcumuladoParada.setText('0:00:00')
            QMessageBox.about(self, "Información", "No hay información que cumpla con el criterio de filtrado")
        else:
            self.ui.lineTiempoAcumuladoParada.setText(str(tiempoAcumuladoEnTerminal))

    def borrarParada(self):
        self.ui.comboChoferParada.setCurrentIndex(0)
        self.ui.comboCocheParada.setCurrentIndex(0)
        self.ui.comboParadaParada.setCurrentIndex(0)
        self.ui.dateFechaParada.setDate(QDate.currentDate())
        self.ui.tableParada.setRowCount(0);
        self.ui.lineTiempoAcumuladoParada.setText('0:00:00')

    def queryPersonal(self):
        """Solicitud del Personal en el sistema."""
        global dataPersonalNombre
        global dataPersonalID

        respData = query(method="personal")

        dataPersonalNombre = re.findall(r'<data.*?nombre="(.*?)".*?codigo="(.*?)".*?/>', str(respData))
        dataPersonalNombre = dict(dataPersonalNombre)
        dataPersonalNombre['Ninguno'] = 'Ninguno'
        dataPersonalID = {v: k for k, v in dataPersonalNombre.items()}

        self.ui.comboChoferParada.addItem('Ninguno')
        for data in sorted(dataPersonalNombre.keys(), key=str.lower):
            self.ui.comboChoferParada.addItem(data)
            self.ui.comboChoferRecorrido.addItem(data)

    def queryRecorrido(self):
        """Consulta por el recorrido configurado en la GUI."""
        tiempoInicioRecorrido = self.ui.timeInicioRecorrido.time().toString("HH:mm:ss")
        tiempoFinalRecorrido = self.ui.timeFinalRecorrido.time().toString("HH:mm:ss")
        servicioFecha = self.ui.dateFechaRecorrido.date().toString("yyyy-MM-dd")
        minutosVuelta = self.ui.spinDuracionTotalRecorrido.value()
        minutosAntes = self.ui.spinTiempoAntesRecorrido.value()
        minutosDespues = self.ui.spinTiempoDespuesRecorrido.value()
        chofer = dataPersonalNombre[self.ui.comboChoferRecorrido.currentText()]

        print(tiempoInicioRecorrido)
        print(tiempoFinalRecorrido)
        print(servicioFecha)
        print(minutosVuelta)
        print(minutosAntes)
        print(minutosDespues)
        print(chofer)

        qRecorrido = servidorActual + "xml_string=%3Cmethod%20name=%22transaccion%22%20key=%22" + key + "%22%3E%3Cparams%20oper=%22select%22%20fecha_desde=%22" + servicioFecha + "%20" + tiempoInicioRecorrido + "%22%20chofer=%22" + chofer + "%22%20%3E%3C/params%3E%3C/method%3E"
        print(qRecorrido)
        req = urllib.request.Request(qRecorrido)
        resp = urllib.request.urlopen(req)
        respData = resp.read()

        with open("dataRecorridoRaw.txt", "w") as rawDataFile:
            rawDataFile.write(str(respData))

        #dataRecorrido = re.findall(r'<data  mojon="(.*?)".*?latitud="(.*?)" longitud="(.*?)" localidad="(.*?)".*?'
        #                     r'entorno="(.*?)" codigo="(.*?)" />', str(respData))
        #print(dataRecorrido)


    def borrarRecorrido(self):
        """Seteo de los valores de la pagina de recorrido por defecto."""
        pass

    def queryTarjeta(self):
        """Solicitud de las tarjetas generadas."""
        dni = str(self.ui.lineDocumentoTarjetas.text())
        comprobanteNombre = self.ui.comboComprobanteTarjetas.currentText()

        if dni != '' and comprobanteNombre == 'Ninguno':  # Consulta por DNI
            respData = query(method="tarjeta", dni=dni)
        elif dni == '' and comprobanteNombre != 'Ninguno':  # Consulta por Comprobante
            respData = query(method="tarjeta", comprobanteNombre=comprobanteNombre)
        elif dni != '' and comprobanteNombre != 'Ninguno':  # Consulta por DNI y Comprobante
            respData = query(method="tarjeta", comprobanteNombre=comprobanteNombre, dni=dni)
        else: # Consulta por todas las tarjetas
            opcion = QMessageBox.question(self, "Información", "La consulta que quiere realizar es sobre todas las "
                                                               "tarjetas existentes en el sistema. "
                                                               "¿Esta seguro que desea continuar?",
                                          QMessageBox.Yes | QMessageBox.No)
            if opcion == QMessageBox.Yes:
                respData = query(method="tarjeta")
            else:
                respData = ""

        if "affected=\"0\"" in str(respData):
            QMessageBox.about(self, "Información", "No hay información que cumpla con el criterio de búsqueda")
            self.ui.tableTarjeta.setRowCount(0);
        else:
            dataTarjetas = re.findall(r'<data dni="(\d*)".*?fecha_vto="(.*?)".*?fecha_alta="(.*?)" '
                                      r'usuario_alta="(.*?)".*?comprobante_nombre="(.*?)".*?'
                                      r'serie="(.*?)" />', str(respData))

            i = 0
            j = 0

            header = ['DNI', 'Vencimiento', 'Alta fecha', 'Alta por', 'Comprobante', 'Serie tarjeta']
            self.ui.tableTarjeta.setRowCount(len(dataTarjetas))
            self.ui.tableTarjeta.setColumnCount(len(header))
            self.ui.tableTarjeta.setHorizontalHeaderLabels(header)
            for i in range(len(dataTarjetas)):
                for j in range(len(header)):
                    self.ui.tableTarjeta.setItem(i, j, QTableWidgetItem(dataTarjetas[i][j]))
                    j += 1
                i += 1

    def borrarTarjeta(self):
        """Seteo de los valores de la pagina de tarjetas por defecto."""
        self.ui.lineDocumentoTarjetas.setText('')
        self.ui.comboComprobanteTarjetas.setCurrentIndex(0)
        self.ui.tableTarjeta.setRowCount(0);

    def queryVehiculos(self):
        """Solicitud de los Vehiculos en el sistema."""
        global dataVehiculos

        respData = query(method="vehiculo")

        dataVehiculos = re.findall(r'<data.*?nombre="(.*?)".*?codigo="(.*?)".*?/>', str(respData))
        dataVehiculos = dict(dataVehiculos)
        dataVehiculos['Ninguno'] = 'Ninguno'

        self.ui.comboCocheCoches.addItem('Ninguno')
        self.ui.comboCocheParada.addItem('Ninguno')
        for data in sorted(dataVehiculos.keys()):
            self.ui.comboCocheParada.addItem(data)
            self.ui.comboCocheCoches.addItem(data)


def completarTabla(tabla, headerFila, headerColumna, datos):
    """Completado automático de tabla tipo QTableWidget."""
    # Existen problemas para tablas con distinta cantidad de elementos en sus distintas columnas.
    # La tabla para funcionar actualmente debe estar completa, no puede haber listas con menos elementos.
    tabla.setRowCount(len(datos[0]))
    # print("Filas", len(datos[0]))
    tabla.setColumnCount(len(datos))
    # print("Columnas", len(datos))
    tabla.setHorizontalHeaderLabels(headerColumna)
    if headerFila != 'None':
        tabla.setVerticalHeaderLabels(headerFila)

    for fila in range(len(datos[0])):
        for columna in range(len(datos)):
            tabla.setItem(fila, columna, QTableWidgetItem(str(datos[columna][fila])))


def query(**methodQueryData):
    """Metodo de consulta general."""
    global key

    try:
        if("get_key" in methodQueryData.values()):
            queryString = "xml_string=<method name=\"{method}\"><params oper=\"select\" usuario=\"{usuario}\" password=\"{password}\"></params></method>".format(**methodQueryData)

        elif("transaccion" in methodQueryData.values()):
            if("chofer" not in methodQueryData.keys() and "coche" in methodQueryData.keys()):
                queryString = "xml_string=<method name=\"{method}\" key=\"{authKey}\"><params oper=\"select\" servicio_fecha=\"{servicioFecha}\" coche=\"{coche}\"></params></method>".format(authKey=key, **methodQueryData)
            elif("chofer" in methodQueryData.keys() and "coche" not in methodQueryData.keys()):
                queryString = "xml_string=<method name=\"{method}\" key=\"{authKey}\"><params oper=\"select\" servicio_fecha=\"{servicioFecha}\" chofer=\"{chofer}\"></params></method>".format(authKey=key, **methodQueryData)
            else:
                queryString = "xml_string=<method name=\"{method}\" key=\"{authKey}\"><params oper=\"select\" servicio_fecha=\"{servicioFecha}\" chofer=\"{chofer}\" coche=\"{coche}\"></params></method>".format(authKey=key, **methodQueryData)

        elif("tarjeta" in methodQueryData.values()):
            if("comprobanteNombre" not in methodQueryData.keys() and "dni" not in methodQueryData.keys()):
                queryString = "xml_string=<method name=\"{method}\" key=\"{authKey}\"><params oper=\"select\"></params></method>".format(authKey=key, **methodQueryData)
            elif("comprobanteNombre" not in methodQueryData.keys() and "dni" in methodQueryData.keys()):
                queryString = "xml_string=<method name=\"{method}\" key=\"{authKey}\"><params oper=\"select\" dni=\"{dni}\"></params></method>".format(authKey=key, **methodQueryData)
            elif("comprobanteNombre" in methodQueryData.keys() and "dni" not in methodQueryData.keys()):
                queryString = "xml_string=<method name=\"{method}\" key=\"{authKey}\"><params oper=\"select\" comprobante_nombre=\"{comprobanteNombre}\"></params></method>".format(authKey=key, **methodQueryData)
            elif("comprobanteNombre" and "dni" in methodQueryData.keys()):
                queryString = "xml_string=<method name=\"{method}\" key=\"{authKey}\"><params oper=\"select\" comprobante_nombre=\"{comprobanteNombre}\" dni=\"{dni}\"></params></method>".format(authKey=key, **methodQueryData)

        else:
            if("serie" not in methodQueryData.values()):
                methodQueryData["serie"] = ""
            if("localidad" not in methodQueryData.values()):
                methodQueryData["localidad"] = ""
            if("fechaDesde" not in methodQueryData.values()):
                methodQueryData["fechaDesde"] = ""
            if("horaDesde" not in methodQueryData.values()):
                methodQueryData["horaDesde"] = ""
            queryString = "xml_string=<method name=\"{method}\" key=\"{authKey}\"><params oper=\"select\" serie=\"{serie}\" fecha_desde=\"{fechaDesde} {horaDesde}\"></params></method>".format(authKey=key, **methodQueryData)

        queryString = urllib.parse.quote(queryString, safe="\=")
        queryToServer = servidorActual + queryString
        req = urllib.request.Request(queryToServer)
        resp = urllib.request.urlopen(req)
        respData = resp.read()
        return respData

    except Exception as error:
        print("Se encontró el siguiente error mientras se procesaba la consulta al servidor:", str(error))


def run():
    """Ejecucion de la parte grafica del programa."""
    global app
    app = QApplication(sys.argv)
    login = Ingreso()

    if login.exec_() == QDialog.Accepted:
        main = MainWindow()
        main.show()
        sys.exit(app.exec_())


if __name__ == "__main__":
    try:
        global servidorActual
        global servidorPrincipal
        global servidorSecundario

        servidorPrincipal = "http://190.220.132.134:8001/megaweb/soap/server.php?"
        servidorSecundario = "http://186.138.49.102:8001/megaweb/soap/server.php?"
        servidorActual = servidorPrincipal
        run()

    except Exception as e:
        print("Se ha encontrado un error:", str(e), "Revise el archivo de loggeo \"error.log\".")
        error = int(time.time())
        error = str(datetime.fromtimestamp(error).strftime('%Y-%m-%d %H:%M:%S')) + " - " + str(e)
        with open("error.log", "a") as errorLogFile:
            errorLogFile.write(error)
            errorLogFile.write('\n')
