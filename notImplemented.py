def queryServicios():
    """Solicitud de los servicios programados en el sistema."""
    qServicios = servidor + "xml_string=%3Cmethod%20name=%22servicio%22%20key=%22" + key + \
                 "%22%3E%3Cparams%20oper=%22select%22%20%3E%3C/params%3E%3C/method%3E"
    req = urllib.request.Request(qServicios)
    resp = urllib.request.urlopen(req)
    respData = resp.read()
    dataServicios = re.findall(r'<data tarifa="(.*?)" localidad="(.*?)" sentido="(.*?)".*?salida="(.*?)"'
                               r'.*?codigo="(.*?)".*?/>', str(respData))

def queryGPS():
    """Solicitud de auditoria de GPS."""
    serie = "2350"  # XXXX
    fechaDesde = "2016-05-09"  # AAAA-MM-DD
    horaDesde = "05:00:00"  # HH:MM:SS
    respData = query(method="gpstracking", serie=serie, fechaDesde=fechaDesde, horaDesde=horaDesde)
    # qGPSTracking = servidor + "xml_string=%3Cmethod%20name=%22gpstracking%22%20key=%22" + key + \
    #                "%22%3E%3Cparams%20oper=%22select%22%20serie=%22" + serie + "%22%20fecha_desde=%22" + fechaDesde + \
    #                "%20" + horaDesde + "%22%3E%3C/params%3E%3C/method%3E"
    # print(qGPSTracking)
    # %20fecha_desde=%222016-02-17%2015:00:00%22%3E%3C/params%3E%3C/method%3E
    # %20mayor=%2214000000%22%20estado_equipo=%2280%22%20estado_gps=%2265%22%3E%3C/params%3E%3C/method%3E
    dataGPSTracking = re.findall(r'<data  latitud="(.*?)" velocidad="(.*?)" longitud="(.*?)".*?coche="(.*?)"'
                                 r'.*?fecha="(.*?)".*?/>', str(respData))
    latitudes = re.findall(r'<data  latitud="(.*?)".*?/>', str(respData))
    # print(latitudes)
    # latitudesConvertidas = conversionCoordenadas(latitudes)
    # print(latitudesConvertidas)
    # print(50*"#")
    longitudes = re.findall(r'<data.*?longitud="(.*?)".*?/>', str(respData))
    # print(len(latitudes) == len(longitudes))
    # print(longitudes)
    # longitudesConvertidas = conversionCoordenadas(longitudes)
    # print(longitudesConvertidas)
    # print(50*"#")
    latitudesConvertidas = []
    longitudesConvertidas = []
    coordenadas = []
    latitudesConvertidas = conversionCoordenadas(latitudes[100])
    longitudesConvertidas = conversionCoordenadas(longitudes[100])
    # print(latitudesConvertidas)
    # print(longitudesConvertidas)
    # print(len(latitudes))
    # for i in range(len(latitudes)):
    #    latitudesConvertidas[i] = conversionCoordenadas(latitudes[i])
    #    longitudesConvertidas[i] = conversionCoordenadas(longitudes[i])
        # coordenadas[i] = [latitudesConvertidas[i], longitudesConvertidas[i]]
        # print(type(coordenadas))
        # print(coordenadas[i])
    #    print(latitudesConvertidas[i])
    #    print(longitudesConvertidas[i])
    # coordenadas = [latitudesConvertidas, longitudesConvertidas]
    # print(50*"#")
    # print(coordenadas)
    # print(50*"#")
    saveFile = open('dataGPSTracking.txt', 'w')
    saveFile.write(str(dataGPSTracking))
    saveFile.close()
    saveFile = open('dataGPSTrackingCruda.txt', 'w')
    saveFile.write(str(respData))
    saveFile.close()
    saveFile = open('dataGPSTrackingCoordenadas.txt', 'w')
    saveFile.write(str(latitudesConvertidas) + ", " + str(longitudesConvertidas))
    saveFile.close()
    # print("GPS")
    i = 0
    for data in dataGPSTracking:
        # print(i, "-", "Latitud:", data[0], "- Longitud:", data[2], "- Velocidad:", data[1], "- n° de coche:", data[3], "- Fecha:", data[4])
        i += 1

def conversionCoordenadas(gms):
    """Conversion de coordenadas GMS a Grados decimales."""
    partes = re.split('[-.]', gms)
    partes[0] = float(partes[0][2::])
    print(partes[0])
    partes[1] = float(partes[1])
    print(partes[1])
    partes[2] = float(partes[2][0:4])/10000
    print(partes[2])
    coordenada = -(partes[0] + (partes[1] + partes[2])/60)
    # print(partes[0])
    # print(partes[1])
    # print(partes[20servicio=%22" + servi2])
    # Redondear los numeros a una resolucion menor.
    # print(coordenada)
    return coordenada

#def queryPuntosGPS(self):
#    """Solicitud de los puntos GPS programados en el sistema."""
#    global dataGPS
#    mojon = "3"

#    qGPS = servidor + "xml_string=%3Cmethod%20name=%22punto_gps%22%20key=%22" + key + \
#           "%22%3E%3Cparams%20oper=%22select%22%20mojon=%22" + mojon + "%22%3E%3C/params%3E%3C/method%3E"
#    req = urllib.request.Request(qGPS)
#    resp = urllib.request.urlopen(req)
#    respData = resp.read()
#    print(respData)

#    dataGPS = re.findall(r'<data  mojon="(.*?)".*?latitud="(.*?)" longitud="(.*?)" localidad="(.*?)".*?'
#                         r'entorno="(.*?)" codigo="(.*?)" />', str(respData))


#    qGPSTracking = servidor + "xml_string=%3Cmethod%20name=%22gpstracking%22%20key=%22" + key + \
#                   "%22%3E%3Cparams%20oper=%22select%22%20serie=%22" + serie + "%22%20fecha_desde=%22" + fechaDesde + \
#                   "%20" + horaDesde + "%22%3E%3C/params%3E%3C/method%3E"
#    qServicios = servidor + "xml_string=%3Cmethod%20name=%22servicio%22%20key=%22" + key + \

    #dataGPSTracking = re.findall(r'<data  latitud="(.*?)" velocidad="(.*?)" \
    #    longitud="(.*?)".*?coche="(.*?)".*?fecha="(.*?)".*?/>', str(respData))
    # latitudes = re.findall(r'<data  latitud="(.*?)".*?/>', str(respData))
    # longitudes = re.findall(r'<data.*?longitud="(.*?)".*?/>', str(respData))
    # latitudes = dataGPSTracking[::][0]
    # longitudes = dataGPSTracking[::][2]
    # print(latitudes)
    # print(longitudes)
    #print("GPS")
    #i = 1
    # for data in dataGPSTracking:
    # """ print(i, "-", "Latitud:", data[0], "- Longitud:", data[2], "- Velocidad:", data[1], "- n de coche:", data[3],
    #  "- Fecha:", data[4])"""
    #     i += 1

    # ACOMODAR PARA TENER CONTROL DE ERRORES
    # dataError = re.findall(r'errorcode="(.*?)".*?/>', str(respData))
    # dataError = dataError[0]
    # if dataError:
    #     if dataError == "1000":
    #         print("Codigo de error: ", dataError)
    #         print("Consulta invalida. Por atributos con valores invalidos.")
    #     elif dataError == "1001":
    #         print("Codigo de error: ", dataError)
    #         print("Metodo u operacion no valido")
    #     elif dataError == "1030":
    #         print("Codigo de error: ", dataError)
    #         print("Valor invalido de un atributo")
    #     elif dataError == "1040":
    #         print("Codigo de error: ", dataError)
    #         print("Clave invalida o vencida")
    #     elif dataError == "1050":
    #         print("Codigo de error: ", dataError)
    #         print("Error de conexion")

