# TODO CÓDIGO
- Generar la captura y analisis de argumentos de inicio del programa para agregar un modo verbose o debug.
- Generar la posibilidad de escribir reportes y sacar los datos a archivos para otro tipo de procesamiento.
- Pasar todas las querys a la funcion de query.
- Hacer que la funcion de query general se encargue de manejar los datos recibidos, no la informacion en si, si no los errores.
- Generar una función que se encargue de llenar las tablas con los valores de header, longitudes e indices pasados.
- Revisar si la estructura de las expresiones regulares no puede modificarse para que genere directamente diccionarios, listas o tuplas segun corresponda.
- Copiar las tablas a Calc o similar.
- Agregar variables locales que faciliten la lectura posterior del programa. Por ejemplo si 0 es el indice de una lista o tupla para comprobante, hacer:
comprobante = 0, para luego poder hacer lista[comprobante] en lugar de lista[0].
- Revisar las funciones para optimizarlas, se hace varias veces lo mismo en varios lugares diferentes, diagramar el programa.
- Supuestamente lo que se está usando para la distribución de programas es pip. (Cierto, pero utilizado para distribuir entre desarrolladores y no a usuario final).
- Tener en cuenta el error de inicio de sesión (o quizás el de alguna consulta) por falla de conexión o superación de los tiempos de conexión.
- Agregar la condición de '' (además de 'Ninguno') a la búsqueda de tarjeta y/o coche (o donde sea que aplique).
- En las tablas que no permitan calcular el tiempo en terminal por un error de generación de la información advertir al usuario y tomar una fila menos para el cálculo.
- Si se presiona "Filtrar" sin haber realizado previamente una consulta se genera una excepción.
- Revisar la longitud de los datos filtrados según la parada y notificar si no existe información bajo esos parámetros de filtrado.


# TODO GRÁFICO
- Ocultar la barra de menú y traer con Alt.
- Generar un cambio de color de las celdas en función de cierto parámetro de control.
- Confirmacion de cierre para la salida del programa desde la X de la ventana. O mediante Alt + F4.
- Poner algún tipo de notificación del estado del servidor, base de datos, etc.
- Thread de consulta separado. No dentro de la clase QMainWindow.


# HECHO
- La tabla de tarjetas no tiene habilitado el sorting.
- Filtrar las tarjetas por el nombre/código del comprobante.
- Rotar los diccionarios, que la clave sea el número de código y el valor el nombre en cuestión, y que la clave sea el número (1) y no str ('1'). [VER]
Esto se debe a que en micronauta puede haber dos entradas con distinto ID pero con el mismo contenido, pero no puede haber dos indices con el mismo valor.
Los diccionarios en python funcionan de la misma manera y sería muy bueno que tuviera el mismo comportamiento.
- El problema de lo anterior es cuando hay que realizar busquedas por key y por value. Para esto hay dos soluciones, o se generan dos diccionarios
donde cada uno sea util en un caso de busqueda o se reemplaza toda la estructura por tuplas o listas. Las tuplas y las listas tienen la ventaja
de estar ordenadas pero tienen la desventaja de que las busquedas son mas lentas. Además las listas son mutables y las tuplas no.
Por ejemplo se podria hacer dataLocalidadIndiceID = {'1': 'VN', ...} y dataLocalidadIndiceNombre = {'VN': '1', ...}.
- No funciona en lineas para las que el CT NO es su terminal. Por ejemplo Línea 11.
- Cambiar los nombres de los comprobantes por nombres válidos en Micronauta, o encontrar el tipo de codificación que tienen.
- Ver de corregir la forma en la que se agrega el header al archivo .xls exportado.
- Generar todos los casos de busqueda para la pestaña de parada.
