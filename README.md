# Control de tiempo

Software para el control de horarios de las líneas de colectivos urbanos e interurbanos de la empresa Trans-Bus S.A. y La Estrella S.A. de la ciudad de Villa María y Villa Nueva.

# Requisitos para el uso

- Cuenta en el sistema de Micronauta habilitada con los permisos necesarios para consultar las categorías necesarias

# Requisitos de software

- Python 3
- PyQt 4
- librería para el manejo de planillas de cálculo desde Python
- Windows, Linux o MacOs